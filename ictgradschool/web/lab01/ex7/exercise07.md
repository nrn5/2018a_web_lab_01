###Herbivorous
* Horse
* Cow
* Sheep
* Chicken
* Capybara

###Carnivorous
* Cat
* Dog
* Polar Bear
* Frog
* Hawk
* Lion
* Fox
* Fennec Fox

###Omnivorous
* Rat
* Raccoon
* Chicken
* Fennec Fox
